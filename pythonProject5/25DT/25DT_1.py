# 导入数据分析常用库
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix, roc_curve, roc_auc_score

# 导入数据
heart = pd.read_csv('processed cleveland4.csv')

# 采用get_dummies()编码方式处理非连续性分类数据

cp_dummies= pd.get_dummies(heart['cp'],prefix = 'cp')
restecg_dummies =  pd.get_dummies(heart['restecg'],prefix='restecg')
slope_dummies =  pd.get_dummies(heart['slope'],prefix='slope')
thal_dummies = pd.get_dummies(heart['thal'],prefix='thal')

# 将原数据中经过独热编码的列删除
heart_new =  heart.drop(['cp','restecg','slope','thal'],axis=1)
heart_new = pd.concat([heart_new,cp_dummies,restecg_dummies,slope_dummies,thal_dummies],axis=1)
heart_new.head()

# 分离出数据和标签
label =  heart_new['target']
data =  heart_new.drop('target',axis=1)
data.shape
(303, 23)

# 数据集合的不同特征之间数据相差有点大，对于SVM、KNN等算法，会产生权重影响，因此需要归一化处理数据
from sklearn.preprocessing import StandardScaler
standardScaler = StandardScaler()
standardScaler.fit(data)
data =  standardScaler.transform(data)

# 拆分训练集，测试集
from sklearn.model_selection import train_test_split, GridSearchCV

train_X,test_X,train_y,test_y = train_test_split(data,label,random_state=3)
train_X.shape
(227, 23)

# 构建模型
from sklearn.tree import DecisionTreeClassifier
tree = DecisionTreeClassifier()

# 训练数据
tree.fit(train_X,train_y)

# 预测数据
pred_y = tree.predict(test_X)

# 评估模型
accuracy_score(test_y,pred_y)
0.76315789473684215

tree.score(train_X,train_y)
1.0

tree.score(test_X,test_y)
0.76315789473684215

param_test={'max_features':['auto','sqrt','log2'],
            'min_samples_split':list(range(2,20)),
            'min_samples_leaf':list(range(1,12))

}
tree_gv = GridSearchCV(estimator=tree,param_grid=param_test,cv=5)
tree_gv.fit(train_X,train_y)

# 最优参数
tree_gv.best_params_
{'max_features': 'auto', 'min_samples_leaf': 9, 'min_samples_split': 12}

# 预测数据
pred_y = tree_gv.predict(test_X)

# 查看主要分类指标文本报告
print(classification_report(test_y,pred_y))

i=1
fig1= plt.figure(figsize=(3*5,1*4))

estimator_dict={'Decision Tree':tree_gv}
for key,estimator in estimator_dict.items():

    #绘制混淆矩阵
    pred_y =  estimator.predict(test_X)
    matrix = pd.DataFrame(confusion_matrix(test_y,pred_y))
    ax1 = fig1.add_subplot(1,3,i)
    sns.heatmap(matrix,annot=True,cmap='OrRd')
    plt.title('Confusion Matrix -- %s ' % key)
    i+=1
plt.show()

tree_score =  tree_gv.predict_proba(test_X)[:,1]
score = [tree_score]
i=0
j=1
fig2 = plt.figure(figsize=(4*5,1*4))

for key,estimator in estimator_dict.items():
    pred_y =  estimator.predict(test_X)
    ax2 = fig2.add_subplot(1,4,j)
    fprs,tprs,thresholds = roc_curve(test_y,score[i])
    plt.plot(fprs,tprs)
    plt.plot([0,1],linestyle='--')
    area = roc_auc_score(test_y,pred_y)
    plt.xlabel('FP rate\n %s_AUC:%f' % (key,area),fontsize=12)
    plt.ylabel('TP rate',fontsize=12)
    plt.title('ROC of %s 曲线 ' % key,fontsize=14)
    plt.grid()
    i +=1
    j +=1

    # 添加网格线
    plt.grid()
    plt.legend()
plt.show()

# 随机森林
x = heart.drop(['target'], axis=1)
y = heart['target']
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=20)
rfc = RandomForestClassifier(n_estimators=200)
rfc.fit(x_train, y_train)
y_pred = rfc.predict(x_test)
# 计算准确率
score4 = rfc.score(x_test, y_test)
print("随机森林分类准确率为：\n", score4)
# 查看精确率、召回率、F1-score
report = classification_report(y_test, y_pred, labels=[0, 1], target_names=['Not sick', 'sick'])
print(report)