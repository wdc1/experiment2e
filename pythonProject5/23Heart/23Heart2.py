import pandas as pd
# 读取心脏病数据集
data1 = pd.read_csv("/processed cleveland.csv")
# 查找重复值
duplicates = data1[data1.duplicated()]
# 打印重复值
print("重复值：")
print(duplicates)

# 处理重复值
data1.drop_duplicates(inplace=True)
# 打印处理后的数据集
print("处理后的数据集：")
print(data1)
data1.to_csv('processed cleveland1.csv',index=False)