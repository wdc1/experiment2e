import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# 加载预处理后的UCI心脏数据集
data = pd.read_csv("/processed cleveland4.csv")

# 创建多维模型结构，以性别、胸痛类型和最大心率为纬度，以患病情况为度量
cube = pd.pivot_table(data, values='target', index=['sex', 'cp', 'thalach'])#values结果值为平均值

# 使用切片的方法分析“男性的心脏病患病情况”
male_slice = cube.xs(1, level='sex')
plt.scatter(male_slice.index.get_level_values('cp'), male_slice.index.get_level_values('thalach'), c=male_slice.values, cmap='coolwarm')
plt.xlabel('Chest Pain Type')
plt.ylabel('Thalach')
plt.title('Male Heart Disease Cases')
plt.colorbar(label='Disease Cases')
plt.show()

# 使用切块的方法分析“男性胸痛类型为4，最大心率为160到180之间的心脏病患病情况”
male_chunk = cube.xs((1, 4, slice(160, 181)), level=('sex', 'cp', 'thalach'))
plt.scatter(male_chunk.index.get_level_values('thalach'), male_chunk.values)
plt.xlabel('Thalach')
plt.ylabel('Disease Cases')
plt.title('Male Chest Pain Type 4, Thalach 160-180 Heart Disease Cases')
plt.show()

