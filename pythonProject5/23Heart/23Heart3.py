import numpy as np
import pandas as pd

# 读取心脏病数据集
data = pd.read_csv("/processed cleveland1.csv")

# 查找缺失值(自定义缺失值先替换成空值，同空值一起查找出并处理)
df = data.replace(to_replace='?',value=np.nan)
missing_values = df.isnull().sum()
# 打印缺失值统计
print("缺失值统计：")
print(missing_values)

# 处理缺失值
df.dropna(inplace=True)
# 打印处理后的数据集
print("处理后的数据集：")
print(df)
df.to_csv('processed cleveland2.csv',index=False)