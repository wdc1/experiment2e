# 导入数据分析常用库
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# 导入数据
heart = pd.read_csv('processed cleveland4.csv')

# 采用get_dummies()编码方式处理非连续性分类数据

cp_dummies= pd.get_dummies(heart['cp'],prefix = 'cp')
restecg_dummies =  pd.get_dummies(heart['restecg'],prefix='restecg')
slope_dummies =  pd.get_dummies(heart['slope'],prefix='slope')
thal_dummies = pd.get_dummies(heart['thal'],prefix='thal')

# 将原数据中经过独热编码的列删除
heart_new =  heart.drop(['cp','restecg','slope','thal'],axis=1)
heart_new = pd.concat([heart_new,cp_dummies,restecg_dummies,slope_dummies,thal_dummies],axis=1)
heart_new.head()

# 分离出数据和标签
label =  heart_new['target']
data =  heart_new.drop('target',axis=1)
data.shape
(303, 23)

# 数据集合的不同特征之间数据相差有点大，对于SVM、KNN等算法，会产生权重影响，因此需要归一化处理数据
from sklearn.preprocessing import StandardScaler
standardScaler = StandardScaler()
standardScaler.fit(data)
data =  standardScaler.transform(data)

# 拆分训练集，测试集
from sklearn.model_selection import train_test_split
train_X,test_X,train_y,test_y = train_test_split(data,label,random_state=3)
train_X.shape
(227, 23)

from sklearn.linear_model import LogisticRegression
log_reg= LogisticRegression()

# 训练模型
log_reg .fit(train_X,train_y)

# 预测数据
log_pred_y = log_reg.predict(test_X)

# 评估模型
log_reg.score(train_X,train_y)
0.85462555066079293

log_reg.score(test_X,test_y)
0.88157894736842102

# 导入评价指标，分类准确率
from sklearn.metrics import accuracy_score, confusion_matrix, roc_curve, roc_auc_score

accuracy_score(test_y,log_pred_y)
0.88157894736842102

# 导入网格搜索
from sklearn.model_selection import GridSearchCV
param_test =  {'penalty':['l2','l1'],
                'C':[0.01,0.1,1.0,10,100],
                'class_weight':[None,'balanced']}
log_gv =  GridSearchCV(estimator=log_reg,param_grid=param_test,cv=5)
log_gv.fit(train_X,train_y)

# 最优模型参数
log_gv.best_params_
{'C': 0.01, 'class_weight': 'balanced', 'penalty': 'l2'}

# 最优模型得分
log_gv.score(train_X,train_y)
0.8590308370044053

log_gv.score(test_X,test_y)
0.86842105263157898

# 预测数据
log_pred_y =  log_gv.predict(test_X)

# 查看主要分类指标文本报告
from sklearn.metrics import classification_report

print(classification_report(test_y,log_pred_y))

i=1
fig1= plt.figure(figsize=(3*5,1*4))

estimator_dict={'Logistic Regression':log_gv}
for key,estimator in estimator_dict.items():
    # 绘制混淆矩阵
    pred_y =  estimator.predict(test_X)
    matrix = pd.DataFrame(confusion_matrix(test_y,pred_y))
    ax1 = fig1.add_subplot(1,3,i)
    sns.heatmap(matrix,annot=True,cmap='OrRd')
    plt.title('Confusion Matrix -- %s ' % key)
    i+=1
plt.show()

log_score =  log_gv.decision_function(test_X)
score = [log_score]
i=0
j=1
fig2 = plt.figure(figsize=(4*5,1*4))

for key,estimator in estimator_dict.items():
    pred_y =  estimator.predict(test_X)
    ax2 = fig2.add_subplot(1,4,j)
    fprs,tprs,thresholds = roc_curve(test_y,score[i])
    plt.plot(fprs,tprs)
    plt.plot([0,1],linestyle='--')
    area = roc_auc_score(test_y,pred_y)
    plt.xlabel('FP rate\n %s_AUC:%f' % (key,area),fontsize=12)
    plt.ylabel('TP rate',fontsize=12)
    plt.title('ROC of %s 曲线 ' % key,fontsize=14)
    plt.grid()
    i +=1
    j +=1

    # 添加网格线
    plt.grid()
    plt.legend()
plt.show()