import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('darkgrid')
import warnings
warnings.filterwarnings('ignore')
import matplotlib
from sklearn import metrics
from scipy.stats import pearsonr
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
plt.rcParams['font.sans-serif']=['SimHei']
plt.rcParams['axes.unicode_minus'] = False


# 加载预处理后的UCI心脏数据集
heart = pd.read_csv("D:\importance\py\pythonProject5\processed cleveland4.csv")
data = heart.copy()
print("预处理后数据集大小：")
print(data.shape)
print()

# 采用get_dummies()编码方式处理非连续性分类数据
cp_dummies= pd.get_dummies(data['cp'],prefix = 'cp')
restecg_dummies =  pd.get_dummies(data['restecg'],prefix='restecg')
slope_dummies =  pd.get_dummies(data['slope'],prefix='slope')
thal_dummies = pd.get_dummies(data['thal'],prefix='thal')

# 将原数据中经过独热编码的列删除
heart_new =  data.drop(['cp','restecg','slope','thal'],axis=1)
heart_new = pd.concat([heart_new,cp_dummies,restecg_dummies,slope_dummies,thal_dummies],axis=1)
print("经独热编码后的数据集前几行：")
print(heart_new.head())
print()

# 分离出数据和标签
label =  heart_new['target']
data =  heart_new.drop('target',axis=1)
#读取数据矩阵长度
print("分离数据和标签后数据集大小：")
print(data.shape)
print()

# 数据集合的不同特征之间数据相差有点大，对于SVM、KNN等算法，会产生权重影响，因此需要归一化处理数据
from sklearn.preprocessing import StandardScaler
standardScaler = StandardScaler()
standardScaler.fit(data)
data =  standardScaler.transform(data)

# 拆分训练集，测试集
from sklearn.model_selection import train_test_split
train_X,test_X,train_y,test_y = train_test_split(data,label,random_state=3)
print("训练集中数据的大小：")
print(train_X.shape)
print()

# 构建模型
from sklearn.neighbors import KNeighborsClassifier
knn =  KNeighborsClassifier()
#训练数据
knn.fit(train_X,train_y)
# 预测数据
knn_pred_y = knn.predict(test_X)
prob_y = knn.predict_proba(test_X)[:, 1]
# 评估模型
print("KNN模型训练集得分：")
print(knn.score(train_X,train_y))

print("KNN模型测试集得分：")
print(knn.score(test_X,test_y))

print("KNN模型测试集准确率：")
print(accuracy_score(test_y,knn_pred_y))
print()

#使用网格搜索寻找更好的模型参数
from sklearn.model_selection import GridSearchCV
knn =  KNeighborsClassifier()
param_test =  [
    {'n_neighbors':[i for i in range(1,31)],
    'weights':['uniform']},

    {'n_neighbors':[i for i in range(1,21)],
    'weights':['distance'],
    'p':[i for i in range(1,6)]}
]
knn_gv = GridSearchCV(estimator = knn,param_grid=param_test,cv=5)
knn_gv.fit(train_X,train_y)

# 最优参数
print("最优参数：")
print(knn_gv.best_params_)
print()

knn =  KNeighborsClassifier(n_neighbors=13,p=5,weights='distance')

#训练数据
knn.fit(train_X,train_y)

# 预测数据
knn_pred_y = knn.predict(test_X)

# 评估模型
print("使用最优参数，得出的模型训练集得分：")
print(knn.score(train_X,train_y))
print()

# 预测数据
knn_pred_y = knn_gv.predict(test_X)

# 查看主要分类指标文本报告
print("主要分类指标文本报告: ")
print(classification_report(test_y,knn_pred_y))
print(knn_gv.score(test_X,test_y))
print()

# 使用均方根误差（RMSE）、皮尔逊相关系数(R)、标准差（STD）评估模型的性能
MSE = metrics.mean_squared_error(test_y,knn_pred_y)
RMSE = np.sqrt(MSE)
R = pearsonr(test_y, knn_pred_y)[0]
std = np.std(test_y-knn_pred_y)
print("模型的RMSE、R、STD分别是：")
print(RMSE,R,std)

from sklearn.metrics import confusion_matrix, roc_curve, auc
#计算混淆矩阵
cm = confusion_matrix(test_y, knn_pred_y)

#计算假正率（FPR）、真正率（TPR）和AUC值：
fpr, tpr, _ = roc_curve(test_y, prob_y)
auc_score = auc(fpr, tpr)
#绘制混淆矩阵
plt.imshow(cm, cmap=plt.cm.Blues)
plt.title('Confusion Matrix -- KNN')
plt.colorbar()
classes = ['0', '1']
tick_marks = np.arange(len(classes))
plt.xticks(tick_marks, classes)
plt.yticks(tick_marks, classes)

thresh = cm.max() / 2.
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        plt.text(j, i, format(cm[i, j], 'd'),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

plt.xlabel('Predicted Label')
plt.ylabel('True Label')
plt.show()

#绘制ROC曲线
plt.plot(fpr, tpr, label='KNN (AUC = %0.2f)' % auc_score)
plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic')
plt.legend(loc="lower right")
plt.show()