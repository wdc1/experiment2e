import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('darkgrid')
import warnings
warnings.filterwarnings('ignore')
import matplotlib
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics
from scipy.stats import pearsonr
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV

# 加载预处理后的UCI心脏数据集
heart = pd.read_csv("/processed cleveland4.csv")
data = heart.copy()

# 划分特征和标签
label = data['target']
data = data.drop('target', axis=1)


# 划分训练集和测试集
train_X, test_X, train_y, test_y = train_test_split(data, label, random_state=3)
print("训练集中数据的大小：")
print(train_X.shape)
print()


# 构建朴素贝叶斯模型
nb = GaussianNB()
# 训练模型
nb.fit(train_X, train_y)
# 预测测试集
nb_pred_y = nb.predict(test_X)
prob_y = nb.predict_proba(test_X)[:, 1]
# 评估模型
print("NB模型训练集得分：")
print(nb.score(train_X,train_y))

print("NB模型测试集得分：")
print(nb.score(test_X,test_y))

print("NB模型测试集准确率：")
print(accuracy_score(test_y,nb_pred_y))
print()


# 使用网格搜索寻找更好的模型参数
param_grid = {'var_smoothing': [1e-9, 1e-8, 1e-7]}
grid_search = GridSearchCV(nb, param_grid, cv=5)
grid_search.fit(train_X, train_y)
#最优参数
best_params = grid_search.best_params_
print("最优参数:\n", best_params)
print()

nb = GaussianNB(var_smoothing=best_params['var_smoothing'])

#训练数据
nb.fit(train_X,train_y)

# 预测数据
nb_pred_y = nb.predict(test_X)

# 评估模型
print("使用最优参数，得出的模型训练集得分：")
print(nb.score(train_X,train_y))
print()

# 预测数据
nb_pred_y = grid_search.predict(test_X)

# 查看主要分类指标文本报告
print("主要分类指标文本报告: ")
print(classification_report(test_y, nb_pred_y))
print(grid_search.score(test_X,test_y))
print()

# 使用均方根误差（RMSE）、皮尔逊相关系数(R)、标准差（STD）评估模型的性能
MSE = metrics.mean_squared_error(test_y, nb_pred_y)
RMSE = np.sqrt(MSE)
R = pearsonr(test_y, nb_pred_y)[0] #评估 NB 模型的预测结果与真实值之间的线性相关程度
std = np.std(test_y-nb_pred_y)
print("模型的RMSE、R、STD分别是：")
print(RMSE,R,std)


from sklearn.metrics import confusion_matrix, roc_curve, auc
#计算混淆矩阵
cm = confusion_matrix(test_y, nb_pred_y)

#计算假正率（FPR）、真正率（TPR）和AUC值：
fpr, tpr, _ = roc_curve(test_y, prob_y)
auc_score = auc(fpr, tpr)
#绘制混淆矩阵
plt.imshow(cm, cmap=plt.cm.Blues)
plt.title('Confusion Matrix -- NB')
plt.colorbar()
classes = ['0', '1']
tick_marks = np.arange(len(classes))
plt.xticks(tick_marks, classes)
plt.yticks(tick_marks, classes)

thresh = cm.max() / 2.
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        plt.text(j, i, format(cm[i, j], 'd'),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

plt.xlabel('Predicted Label')
plt.ylabel('True Label')
plt.show()

#绘制ROC曲线
plt.plot(fpr, tpr, label='NB (AUC = %0.2f)' % auc_score)
plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic')
plt.legend(loc="lower right")
plt.show()